<?xml version="1.0" encoding="utf-8" standalone="no"?>
<?molk title="GNU Emacs Customisation with .emacs" created="2010-01-06" ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../../style/firefox/content.css" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../../style/firefox/base.css" />
    <title>GNU Emacs Customisation with .emacs (Ignored)</title>
  </head>
  <body id="content">
    <!--! Copyright 2009 Morten Lauritsen Khodabocus, info@molk.ch
        Licensed under the GPL: http://molk.ch/licence.txt
      -->
    <p class="summary">Customisation of applications is a two-edged sword. On the one hand, you get to make the tool work closer to the way you would have done it yourself. On the other hand, particularly with a tool like a text editor, you are bound to use it in different locations, and thus, likely, with different levels of customisation.</p>
    <p>When starting up, Emacs will look for a 
    <span class="code">.emacs</span>
    <a href="https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html">initialization file</a> in your home directory. This file can contain 
    <a href="https://www.gnu.org/software/emacs/manual/html_node/elisp/index.html">Emacs Lisp</a> instructions which makes Emacs work like you want it to.</p>
    <h2 id="minimal-customisation">Minimal customisation</h2>
    <p>To minimise the impact of switching between the default and my personal favourite configuration, I try to keep my customisation cosmetic. Rebinding key combinations is at a strict minimum - I would have loved rebinding 
    <span class="code">C-u</span> to 
    <span class="code">undo</span>, but there's a world of difference between that and the default binding ("Do whatever comes next 4 times"), which spells trouble for when I end up in an Emacs without my customisations, so I stuck with the default 
    <span class="code">C-xu</span> for 
    <span class="code">undo</span>.</p>
    <h2 id="stay-in-control">Stay in control</h2>
    <p>I want to know what I'm doing when customising Emacs. Thus, I don't use the 
    <a href="http://www.gnu.org/software/emacs/manual/html_node/emacs/Easy-Customization.html">Easy Customisation Interface</a>, which is nice, but it adds a level of indirection that I would prefer to avoid. It makes changes to my .emacs, and to me, the best way of ensuring that I know what's going on in my .emacs is to only modify it directly.</p>
    <p>Of course, there is no law against making a change using the easy interface, and then manually integrating the change that was made into my own config.</p>
    <h2 id="my-customisation">My customisation</h2>
    <p>Here follows a detailed description of my Emacs customisation file. If you prefer, here's the raw 
    <a href=".emacs">.emacs</a>.</p>
    <h3 id="nillify-c-x-c-c">Nillify 
    <span class="code">C-xC-c</span></h3>
    <p>Remap 
    <span class="code">C-xC-c</span> to 
    <span class="code">C-xc</span>, which is a lot harder to type by mistake:</p>
    <pre class="code">
(global-set-key "\C-x\C-c" nil)
(global-set-key "\C-xc" 'save-buffers-kill-emacs)
</pre>
    <p>I regularly hit 
    <span class="code">C-xC-c</span> by mistake, which is a pain. With 
    <a href="#backup-and-autosave">backup, autosave</a> and 
    <a href="#desktop">Desktop</a>, you don't lose much, but still.</p>
    <h3 id="nillify-c-z">Nillify 
    <span class="code">C-z</span></h3>
    <p>In many other applications, 
    <span class="code">C-z</span> is Undo, which means that with the default Emacs binding, I frequently end up unwillingly minimising - so I prefer rebinding 
    <span class="code">C-z</span> to do nothing:</p>
    <pre class="code">
(global-set-key "\C-z" nil)
</pre>
    <h3 id="add-shortcuts">Add 
    <span class="code">C-zSomething</span> shortcuts</h3>
    <p>Map useful functions which don't have a default binding to 
    <span class="code">C-zSomething</span>. I used to have a couple more of these, because the default bindings are sometimes... Difficult. I guess 
    <span class="code">C-x^</span> might qualify as a shortcut on an american keyboard, but with other layouts, it would be more aptly named "hard-to-hit-cut" - several of the default shortcuts I have never figured out how to type.</p>
    <h4 id="hippie-expand">Hippie expand ( 
    <span class="code">C-zC-z</span>)</h4>
    <p>Enhanced completion at point - does a very imaginative job of guessing what you want:</p>
    <pre class="code">
(global-set-key "\C-z\C-z" 'hippie-expand)
</pre>
    <p>Looks in open buffers, the file system, the kill ring, etc. for a possible completion of the text preceding point. 
    <br />See also: 
    <a href="http://www.xemacs.org/Documentation/packages/html/">The 
    <span class="code">hippie-expand</span> documentation</a>.</p>
    <h4 id="revert-buffer">Revert buffer ( 
    <span class="code">C-zC-r</span>)</h4>
    <p>Reload the file in the current buffer from disk - prompts if there are changes which will be lost:</p>
    <pre class="code">
; revert-buffer reloads the file, prompts if there are changes
(global-set-key "\C-z\C-r" 'revert-buffer)
</pre>
    <p>This function does not seem to be linked to a default shortcut, and autocompletion is little help when invoking it using 
    <span class="code">M-x</span>, since other functions have similar names.</p>
    <h3 id="inhibit-startup-screen">Disable the startup screen ( 
    <span class="code">inhibit-startup-screen</span>)</h3>
    <p>Once you've seen it 50 times, it gets old:</p>
    <pre class="code">
(setq inhibit-startup-screen t)
</pre>
    <h3 id="blink-cursor-mode">Make the point not blink ( 
    <span class="code">blink-cursor-mode</span>)</h3>
    <p>I grew up with an Emacs point that did not blink, it reminds me of youth and for-fun nocturnal hacking:</p>
    <pre class="code">
(blink-cursor-mode nil)
</pre>
    <h3 id="column-number-mode">Display column numbers ( 
    <span class="code">column-number-mode</span>)</h3>
    <p>It's always nice to know exactly where you are in a file - display the current column number in the mode-line:</p>
    <pre class="code">
(column-number-mode t)
</pre>
    <h3 id="disable-mouse-stuff">Menubars, scrollbars, and toolbars are for mouse-users</h3>
    <p>This is a bit extreme, but I know that in Emacs, anything can be done using the keyboard, which IMHO is more efficient. Hiding all the things that make you want to go for the mouse is a good thing in the long term:</p>
    <pre class="code">
(menu-bar-mode nil)
(scroll-bar-mode nil)
(tool-bar-mode nil)
</pre>
    <p>If I really really need the menubar, it is easily reactivated using 
    <span class="code">M-x menu-bar-mode</span>.</p>
    <h3 id="disable-tooltip-mode">Disable tooltip-mode</h3>
    <p>I don't generally use the mouse in Emacs, so tooltips don't get displayed a lot, but when they do, I much prefer seeing them in the echo area:</p>
    <pre class="code">
(tooltip-mode nil)
</pre>
    <p>See also: 
    <a href="http://www.gnu.org/software/emacs/manual/html_node/emacs/Tooltips.html">The Emacs Manual: Tooltips</a>.</p>
    <h3 id="mouse-avoidance-mode">Move the mouse pointer out of the way ( 
    <span class="code">mouse-avoidance-mode</span>)</h3>
    <p>Move the mouse pointer if it is in the way:</p>
    <pre class="code">
(setq mouse-avoidance-mode 'animate)
</pre>
    <p>See also 
    <a href="http://www.gnu.org/software/emacs/manual/html_node/emacs/Mouse-Avoidance.html#Mouse-Avoidance">The Emacs Manual: Mouse Avoidance</a>.</p>
    <h3 id="visible-bell">Use visible rather than audible bell ( 
    <span class="code">visible-bell</span>)</h3>
    <p>I don't like the "error bell" (me and my editor, we play for the same team, and shouldn't beep at each other...), and neither do the people sitting next to me. I prefer visible notification that something went wrong:</p>
    <pre class="code">
(setq visible-bell t)
</pre>
    <h3 id="show-paren-mode">Visually match parentheses ( 
    <span class="code">show-paren-mode</span>)</h3>
    <p>Eventually, the compiler will set you straight, but time is gained when Emacs subtly confirms that you are grouping your statements like you intended to:</p>
    <pre class="code">
(show-paren-mode t)
</pre>
    <h3 id="uniquify-buffer-name-style">Make buffer names unique ( 
    <span class="code">uniquify-buffer-name-style</span>)</h3>
    <p>Sometimes, I work on many files with the same name: log files, index.html files, etc. Display enough of the path to make buffer names unique:</p>
    <pre class="code">
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
</pre>
    <p>See also: 
    <a href="http://www.gnu.org/software/emacs/manual/html_node/emacs/Uniquify.html">The Emacs Manual: Uniquify</a>.</p>
    <h3 id="track-eol">Point stays at the end of lines ( 
    <span class="code">track-eol</span>)</h3>
    <p>When moving point up or down while at the end of a line, stay at the end of the line:</p>
    <pre class="code">
(setq track-eol t)
</pre>
    <p>Saves an occasional 
    <span class="code">C-e</span>.</p>
    <h3 id="size-indication-mode">Display buffer size ( 
    <span class="code">size-indication-mode</span>)</h3>
    <p>Display the buffer size in human readable format:</p>
    <pre class="code">
(size-indication-mode t)
</pre>
    <h3 id="selection-highlighting">Selection highlighting</h3>
    <p>Highlight the current region:</p>
    <pre class="code">
(transient-mark-mode t)
</pre>
    <p>See also: 
    <a href="https://www.gnu.org/software/emacs/manual/html_node/emacs/Mark.html">The Emacs Manual: Transient Mark Mode</a>.</p>
    <h3 id="autosave-bookmarks">Autosave Bookmarks</h3>
    <p>Enable autosaving of 
    <a href="cheat-sheet.html#bookmarks">bookmarks</a>:</p>
    <pre class="code">
(setq bookmark-save-flag 1)
</pre>
    <h3 id="backup-and-autosave">Backup and autosave</h3>
    <p>Write backups and autosave files to a dedicated directory:</p>
    <pre class="code">
(setq version-control t        ; Use version numbers for backups
      kept-new-versions 10     ; Keep the 10 most recent versions
      kept-old-versions 1      ; Also keep the oldest version
      delete-old-versions t    ; Ask to delete excess backup
      backup-by-copying t      ; Copy files, don't rename versions?
      backup-directory-alist '((".*" . "~/.emacs-backup")) ; backups in dedicated dir
)
(defun force-backup-of-buffer ()
      (let ((buffer-backed-up nil))
      (backup-buffer)))

(add-hook 'before-save-hook  'force-backup-of-buffer)
</pre>
    <p>See also 
    <a href="http://www.emacswiki.org/emacs/ForceBackups">Emacs wiki: ForceBackups</a> and 
    <a href="http://www.gnu.org/software/emacs/manual/html_node/tramp/Auto_002dsave-and-Backup.html">The Emacs Manual: Save and Backup</a>.</p>
    <h3 id="desktop">Saving session information (desktop.el)</h3>
    <p>In Emacs 22 and newer, 
    <a href="https://www.emacswiki.org/emacs?action=browse;oldid=DeskTop;id=Desktop">Desktop</a> will startup with the buffers from when Emacs last quit:</p>
    <pre class="code">
(desktop-save-mode 1)
</pre>
  </body>
</html>
