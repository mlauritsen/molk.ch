; Remap C-xC-c to C-xc, which is a lot harder to type by mistake
(global-set-key "\C-x\C-c" nil)
(global-set-key "\C-xc" 'save-buffers-kill-emacs)

; Nillify C-z minimise binding and map useful C-zEtc sequences
(global-set-key "\C-z" nil)

; Provides enhanced completion
(global-set-key "\C-z\C-z" 'hippie-expand)

; Do not display the splash screen
(setq inhibit-startup-screen t)

; Point should not blink
(blink-cursor-mode nil)

; Display the current column number
(column-number-mode t)

; Menubars are for mouse-users
(menu-bar-mode nil)

; Scrollbars are for mouse-users
(scroll-bar-mode nil)

; Toolbars are for mouse-users
(tool-bar-mode nil)

; Display tooltips in the echo area
(tooltip-mode nil)

; Move the mouse pointer if it is in the way
(setq mouse-avoidance-mode 'animate)

; Beeping is offensive to me, notify visibly instead
(setq visible-bell t)

; Highlight matching parenthesis
(show-paren-mode t)

; Display enough of the path to make buffer names unique
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

; When moving point up or down while at an eol, go to the eol of the new line
(setq track-eol t)

; Display the buffer size in human readable format
(size-indication-mode t)

; Highlight the current region
(transient-mark-mode t)

; Backup and autosave
; inspired by:
; http://www.emacswiki.org/emacs/ForceBackups
; http://www.gnu.org/software/emacs/manual/html_node/tramp/Auto_002dsave-and-Backup.html

(setq version-control t	       ; Use version numbers for backups
      kept-new-versions 10     ; Keep the 10 most recent versions
      kept-old-versions 1      ; Also keep the oldest version
      delete-old-versions t    ; Ask to delete excess backup versions?
      backup-by-copying-when-linked t ; Copy linked files, don't rename
backup-directory-alist '((".*" . "~/.emacs-backup")) ; backups in dedicated dir
)
(defun force-backup-of-buffer ()
  (let ((buffer-backed-up nil))
    (backup-buffer)))

(add-hook 'before-save-hook  'force-backup-of-buffer)

; Save bookmarks when they change
; https://www.gnu.org/software/emacs/manual/html_node/emacs/Bookmarks.html
(setq bookmark-save-flag 1)

; Saving session information (desktop.el)
; Desktop will load, at startup, the buffers you were editing when you last quit Emacs.
(desktop-save-mode 1)
