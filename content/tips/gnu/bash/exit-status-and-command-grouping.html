<?xml version="1.0" encoding="utf-8" standalone="no"?>
<?molk title="Exit Status (and Command Grouping)" created="2010-02-03" updated="2010-02-04" ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../../style/firefox/content.css" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../../style/firefox/base.css" />
    <title>Exit Status and Command Grouping (ignored)</title>
  </head>
  <body id="content">
    <!--! Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
      Licensed under the GPL: http://molk.ch/licence.txt
  -->
    <p class="summary">Scripts give concise feedback about what they did by providing an integer status when they finish ("exit"). The exit status of a group of commands depends on the way the commands are grouped.</p>
    <p class="summary">From the source: The GNU bash manual, 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Exit-Status.html">Exit Status</a>, 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Lists.html">Lists</a>, and 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Command-Grouping.html">Command Grouping</a>.</p>
    <h2 id="exit-status">Exit status basics</h2>
    <ul>
      <li>An exit status is between 0 and 255.</li>
      <li>0 means OK (the command succeeded).</li>
      <li>0 &lt; exit status &lt;= 125 are error codes.</li>
      <li>&gt; 125 are reserved for the shell (builtins etc.).</li>
    </ul>
    <h2 id="most-recent-exit-status">What was the exit status? ( 
    <span class="code">$?</span>)</h2>
    <p>When a command exits, the exit status is not written anywhere. However, the exit status is available as 
    <span class="code">$?</span> (only the most recent exit status is available):</p>
    <pre class="code">
ls ~/.emacs
echo $?       # output 0 if the file existed, 2 otherwise
</pre>
    <h2 id="script-exit-status">The exit status of a script</h2>
    <p>Use the 
    <span class="code">exit [n]</span> command to terminate a script:</p>
    <pre class="code">
if test "$#" -ne 3; then
  echo "Usage: $0 a b c" &amp;&gt;2
  exit 1
fi
</pre>
    <ul>
      <li>The argument of the exit command defaults to 
      <span class="code">$?</span>, that is, the exit code of the last command.</li>
      <li>If execution reaches the end of the script file without encountering an 
      <span class="code">exit</span> command, the script exits with the exit status of the last command.</li>
    </ul>
    <h2 id="negating-exit-status">Negating the exit code of a command</h2>
    <p>Prefix a command by 
    <span class="code">!</span> to negate it's exit status - a command that succeeds (exit status 
    <span class="code">0</span>) is negated into failure (exit status 
    <span class="code">1</span>):</p>
    <pre class="code">
! echo "abc"       # exit status 0 becomes exit status 1
</pre>
    <p>A command that fails (exit status 
    <span class="code">2</span>) is negated into success (exit status 
    <span class="code">0</span>), assuming there is no file called 
    <span class="code">abc</span>:</p>
    <pre class="code">
! ls abc           # exit status 2 becomes exit status 0
</pre>
    <p>Since all non-zero exit codes are mapped to 
    <span class="code">0</span>, 
    <span class="code">!</span> is not a 
    <a href="http://en.wikipedia.org/wiki/One-to-one_function">one-to-one</a> function, and once negated, there is no way to deduce the original exit status.</p>
    <h2 id="or">Executing a command if another command fails ("OR")</h2>
    <p>To execute a command only if another command fails, combine them using 
    <span class="code">||</span>:</p>
    <pre class="code">
# if emacs is not available, use vi ;-)
emacs || vi

# if the command fails, output it's exit status
command || echo "$?"
</pre>
    <h2 id="and">Execute a command only if another command succeeds ("AND")</h2>
    <p>A command might only make sense if another command succeeds. Combining them using 
    <span class="code">&amp;&amp;</span> provides this behaviour:</p>
    <pre class="code">
# write ~/.emacs to stdout if it exists
ls ~/.emacs &amp;&amp; cat ~/.emacs

# publish cv.pdf if it does not contain any more TODOs
grep -vq TODO cv.pdf &amp;&amp; publish cv.pdf
</pre>
    <h2 id="exit-status-as-condition">Exit status as the condition in an 
    <a href="if-command.html">if command</a></h2>
    <p>When 
    <a href="if-command.html#simple-conditional">commands are used as conditions</a> in if-commands, it is the exit status that determines whether the condition evaluates to true or false.</p>
    <pre class="code">
# Outputs KO
if ls doesNotExist 2&gt; /dev/null; then
  echo OK;
else
  echo KO;
fi
</pre>
    <h2 id="grouping-commands">Grouping commands</h2>
    <h3 id="sequential-execution">Sequential execution</h3>
    <p>Commands separated by 
    <span class="code">;</span> and/or newline are executed sequentially, and the exit status is the exit status of the last command:</p>
    <pre class="code">
command1 ; command2
</pre>
    <pre class="code">
command1[;]
command2[;]
</pre>
    <p>The second command is executed regardless of the exit status of the first one. Furthermore, when 
    <span class="code">command2</span> runs, the exit status of 
    <span class="code">command1</span> is lost forever, since 
    <span class="code">$?</span> now contains the exit status of 
    <span class="code">command2</span>. Use 
    <a class="code" href="shell-configuration.html#errexit">set -o errexit</a> to be forced to handle all errors.</p>
    <h3 id="multiple-commands-as-one">Executing commands as one in the current shell</h3>
    <p>Multiple commands can be grouped into one logical command by surrounding them by 
    <span class="code">{}</span>-s. The semi-colon at the end is mandatory, and the whitespace is mandatory:</p>
    <pre class="code">
{ command1; command2; }
</pre>
    <p>This is useful to disambiguate complex conditions (using also the 
    <a href="#and">AND</a> and 
    <a href="#or">OR</a> operators previously described):</p>
    <pre class="code">
if { command1 &amp;&amp; command2; } || command3; then
  # ...
fi
</pre>
    <p>Grouping commands like this makes it easy to perform 
    <a href="redirecting-output.html">Redirections</a> on all of them at once:</p>
    <pre class="code">
{ command1; command2; } &amp;&gt; /dev/null
</pre>
    <p>See also: 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Command-Grouping.html">The GNU bash manual: Command Grouping</a>.</p>
    <h3 id="grouping-commands-subshell">Executing commands in a subshell</h3>
    <p>To localise the side effects of a group of commands, surround them by 
    <span class="code">()</span>-s. The whitespace and the last semi-colon are all optional:</p>
    <pre class="code">
( command1; command2[;] )
</pre>
    <p>This works just like 
    <a href="#multiple-commands-as-one">Executing commands as one in the current shell</a>, except that side effects such as changing the current directory or the values of variables are local to the subshell.</p>
    <p>See also: 
    <a href="quoting-arguments.html#command-substitution">Quoting Arguments: Command Substitution</a> and 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Command-Grouping.html">The GNU bash manual: Command Grouping</a>.</p>
  </body>
</html>
