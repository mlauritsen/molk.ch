<?xml version="1.0" encoding="utf-8" standalone="no"?>
<?molk title="Redirecting output" created="2010-02-03" ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../../style/firefox/content.css" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../../style/firefox/base.css" />
    <title>Redirecting output (ignored)</title>
  </head>
  <body id="content">
    <!--! Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
      Licensed under the GPL: http://molk.ch/licence.txt
  -->
    <p class="summary">How to provide standard input to commands, and how to direct output and error messages to the desired destinations. Example:</p>
    <pre class="code">
echo 'hello world' &gt; /dev/null
</pre>
    <p class="summary">Commands have three file descriptors associated with them: 
    <span class="code">stdin</span> (0), 
    <span class="code">stdout</span> (1) and 
    <span class="code">stderr</span> (2).</p>
    <p>From the source: The GNU bash manual, 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Redirections.html">Redirections</a>.</p>
    <h2 id="theory">Redirection theory</h2>
    <p>On paper, this is how redirection works:</p>
    <dl>
      <dt>
        <span class="code">[n]&lt;file</span>
      </dt>
      <dd>Indicates which file 
      <span class="code">stdin</span> should be taken from.</dd>
      <dt>
        <span class="code">[n]&gt;file</span>
      </dt>
      <dd>Indicates which file 
      <span class="code">stdout</span> should be written to. Unless the 
      <span class="code">noclobber</span> shell option is enabled ( 
      <a href="shell-configuration.html">
        <span class="code">set -o noclobber</span>
      </a>), this overwrites the file, even if the command does not write anything to file 
      <span class="code">n</span>.</dd>
      <dt>
        <span class="code">[n]&gt;|file</span>
      </dt>
      <dd>Like 
      <span class="code">&gt;</span>, but overwrites the file even if the 
      <span class="code">noclobber</span> shell option is enabled.</dd>
      <dt>
        <span class="code">[n]&gt;&gt;file</span>
      </dt>
      <dd>If the file does not exist, this works like 
      <span class="code">&gt;</span>. If the file exists, the command output is appended to it.</dd>
      <dt>
      <span class="code">[n]&amp;&gt;file</span> (and 
      <span class="code">[n]&gt;&amp;file</span>)</dt>
      <dd>Redirects both 
      <span class="code">stdout</span> and 
      <span class="code">stderr</span> to the indicated file. This is equivalent to 
      <span class="code">1&gt;file 2&gt;&amp;1</span>.</dd>
      <dt>
        <span class="code">[n]&amp;&gt;&gt;file</span>
      </dt>
      <dd>Like 
      <span class="code">&amp;&gt;file</span>, but appends to the file. This is equivalent to 
      <span class="code">1&gt;&gt;file 2&gt;&amp;1</span>.</dd>
    </dl>
    <p>A couple of notes to these operators:</p>
    <ul>
      <li>For 
      <span class="code">&lt;</span> operators, 
      <span class="code">n</span> defaults to 0 ( 
      <span class="code">stdin</span>).</li>
      <li>For 
      <span class="code">&gt;</span> operators, 
      <span class="code">n</span> defaults to 1 ( 
      <span class="code">stdout</span>).</li>
      <li>Rather than specifying a file, a file descriptor can be given, prefixed by 
      <span class="code">&amp;</span>, like this: 
      <pre class="code">
command 2&gt;&amp;1
</pre> a common mistake is to leave out the 
      <span class="code">&amp;</span> (as in: 
      <span class="code">command 2&gt;1</span>), which will simply redirect 
      <span class="code">stderr</span> to the file 
      <span class="code">${PWD}/1</span>.</li>
      <li>Whitespace is allowed between the operator and the filename ( 
      <span class="code">&gt; file</span>), but not between the file descriptor id and the operator ( 
      <span class="code">2&gt;file</span>).</li>
    </ul>
    <h3 id="location">Redirection location in commands</h3>
    <p>Redirections can occur pretty much anywhere on the commandline - the following three - are equivalent. The middle one should probably be avoided, particularly for more complex commands:</p>
    <pre class="code">
&gt;file ls .
ls &gt;file .
ls . &gt;file
</pre>
    <h3 id="order">The order of redirections is significant</h3>
    <p>If the same file descriptor is redirected several times, only the last (right-most) takes effect:</p>
    <pre class="code">
ls . &gt;file1 &gt;file2   # is equivalent to
ls . &gt;file2
</pre>
    <p>More subtly, when one file descriptor is redirected into another, the order makes a big difference:</p>
    <pre class="code">
cat ~/.emacs 2&gt;&amp;1 1&gt;file     # stderr ends up on stdout, stdout goes to the file
</pre>
    <p>This is not the same as:</p>
    <pre class="code">
cat ~/.emacs 1&gt;file 2&gt;&amp;1     # stdout and stderr both go in the file
</pre>
    <p>The difference being whether 2 is directed to 1 before or after 1 is directed to 
    <span class="code">file</span>.</p>
    <h2 id="stderr">Writing a message to 
    <span class="code">stderr</span></h2>
    <p>Use 
    <span class="code">echo</span> and redirect 
    <span class="code">stdout</span> to 
    <span class="code">stderr</span>:</p>
    <pre class="code">
echo "Wrong input - you are invalid" &gt;&amp;2
</pre>
    <h2 id="silencing-a-command">Forcing a command into silence ( 
    <span class="code">/dev/null</span>)</h2>
    <p>Sometimes commands will insist (in the sense that they do not provide a 
    <span class="code">--quiet</span> option) on telling you things you do not care about. For example, 
    <span class="code">cp</span> will complain if a file to be copied does not exist, but if in that case, you want to do nothing, this will silence it:</p>
    <pre class="code">
cp a b &amp;&gt; /dev/null
</pre>
    <p>Make sure there really are no possible errors that you care about.</p>
    <h2 id="pipelines">Pipelines</h2>
    <p>See the 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Pipelines.html">GNU bash manual: Pipelines</a> (operators 
    <span class="code">|</span> and 
    <span class="code">|&amp;</span>)</p>
  </body>
</html>
