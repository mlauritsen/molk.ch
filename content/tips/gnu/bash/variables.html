<?xml version="1.0" encoding="utf-8" standalone="no"?>
<?molk title="Variables" created="2010-02-02" ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../../style/firefox/content.css" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../../style/firefox/base.css" />
    <title>Variables (ignored)</title>
  </head>
  <body id="content">
    <!--! Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
      Licensed under the GPL: http://molk.ch/licence.txt
  -->
    <p class="summary">In 
    <a href="https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameters">bash terminology</a>, values are stored in parameters, and variables are named parameters used to reference values. For simplicity, this document uses the term "variable" where sometimes it would have been more accurate to say "parameter".</p>
    <p class="summary">From the source: The GNU bash manual, 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Shell-Parameters.html">Shell Parameters</a> and 
    <a href="http://www.gnu.org/software/bash/manual/html_node/Shell-Variables.html">Shell Variables</a>.</p>
    <p class="summary">Related topics: 
    <a href="positional-parameters.html">Positional Parameters</a>, 
    <a href="quoting-arguments.html">Quoting Arguments</a>.</p>
    <h2 id="basics">Variable basics</h2>
    <p>Setting a variable:</p>
    <pre class="code">
MYVAR=123
</pre>
    <p>Unsetting a variable (good practice when it is no longer used):</p>
    <pre class="code">
unset MYVAR
</pre>
    <p>Making variables readonly (improves script readability, since constants can be clearly identified):</p>
    <pre class="code">
declare -r ABC=def

# same as

ABC=def
declare -r ABC

# same as

declare -r ABC
ABC=def
</pre>
    <h2 id="referencing">Referencing variables</h2>
    <p>Variables can be referenced by prefixing the name with 
    <span class="code">$</span>, optionally enclosing the name in 
    <span class="code">{}</span>-s:</p>
    <pre class="code">
var=123
echo $var
echo ${var}
echo "${var}"
</pre>
    <p>Using 
    <span class="code">{}</span>-s often makes references more readable, and avoids ambiguity:</p>
    <pre class="code">
A=1
echo $A2   # references the unset variable A2, which fails or defaults to empty depending on shell settings
echo ${A}2 # writes 12 to stdout
</pre>
    <p>When possible, keep separators outside the variable, to provide a bit of context:</p>
    <pre class="code">
dir=~/site-source
name='fantastic-news'
extension='html'
file="${dir}/${name}.${extension}"

# more readable than

dir=~/site-source/
name='fantastic-news'
extension='.html'
file="${dir}${name}${extension}"
</pre>
    <p>See 
    <a href="quoting-arguments.html">Quoting Arguments</a> (particularly 
    <a href="quoting-arguments.html#shell-parameter-expansion">Shell Parameter Expansion</a>) concerning the effect of quoting on variable references, and for information about all the interesting things you can do with variable references (pattern matching etc.).</p>
    <h2 id="visibility">Variable visibility</h2>
    <p>By default, variables are local to the script they are defined in. When 
    <span class="code">script-A.sh</span> invokes 
    <span class="code">script-B.sh</span>:</p>
    <ul>
      <li>Variables defined in 
      <span class="code">script-A.sh</span> will not be visible in 
      <span class="code">script-B.sh</span>, unless exported: 
      <pre class="code">
export VAR=fornoget
</pre></li>
      <li>After 
      <span class="code">script-B.sh</span> exits, variables defined in 
      <span class="code">script-B.sh</span> will not be visible in 
      <span class="code">script-A.sh</span>, unless the invocation was "sourced": 
      <pre class="code">
source script-B.sh
</pre> when sourced, the contents of 
      <span class="code">script-B.sh</span> are considered to be part of script 
      <span class="code">script-A.sh</span>.</li>
      <li>
      <span class="code">script-A.sh</span> can "pass" a variable with a value local to 
      <span class="code">script-B.sh</span> by assigning it on the same line as the invocation: 
      <pre class="code">
VAR=123 script-B.sh
</pre>
      <span class="code">VAR</span> is unchanged/unset when execution returns to 
      <span class="code">script-A.sh</span>.</li>
    </ul>
    <h3 id="referencing-unset">Referencing unset variables</h3>
    <p>Unless prohibited, it is not an error to reference a variable that does not exist - the following will simply output 4 spaces to stdout:</p>
    <pre class="code">
#!/bin/bash
echo "$none $of $these $variables $exist"
</pre>
    <p>This can be quite confusing, since it becomes impossible to tell a misspelt variable name from a variable which has been assigned the empty string (which could very well be a valid value).</p>
    <p>The classic example, intending to delete the contents of the 
    <span class="code">${trash}</span> directory, but really deleting 
    <span class="code">/*</span>:</p>
    <pre class="code">
rm -rf ${trash}/*
</pre>
    <p>
    <a href="shell-configuration.html#nounset">Disallowing</a> such references makes this mistake much harder to make:</p>
    <pre class="code">
#!/bin/bash
set -o nounset

echo $my_unset_var
</pre>
    <p>Rather than merrily defaulting to an empty value, the shell will report a descriptive error:</p>
    <pre class="code">
&gt; ./script.sh 
./script.sh: line 4: my_unset_var: unbound variable
</pre>
    <p>With this setting, scripts will exit with status 
    <span class="code">1</span> when an unset variable is referenced.</p>
    <h2 id="integer">Integer Variables</h2>
    <p>See the tips on 
    <a href="arithmetic.html#integer-variables">bash Arithmetic</a>.</p>
    <h2 id="appending-text">Appending text to a variable</h2>
    <p>The operator 
    <span class="code">+=</span> provides a simple way to append text to a variable:</p>
    <pre class="code">
VAR=abc
VAR+=def
echo "${VAR}"   # outputs 'abcdef' to stdout
</pre>
    <h2 id="length-of-a-variable">Length of a Variable</h2>
    <p>A "#" before a variable name yields the semantic length of the variable value:</p>
    <pre class="code">
# String variable length
str='elbow'
echo "${#str}" # outputs 5

# Integer variable length
declare -i index=33
echo "${#index}" # outputs 2
</pre>
    <p>See also 
    <a href="arrays.html#length">Array Variable Length</a>.</p>
    <h2 id="empty-vs-unset">Empty vs. unset</h2>
    <p>Quote variable references if an empty value is not the same as an absent value:</p>
    <pre class="code">
DIR=
find ${DIR} # this will list the contents of the current directory

&gt; find "${DIR}" # this will fail, since find does not know what to do with ""
find: cannot search `': No such file or directory
&gt; _
</pre>
    <h2 id="empty">Checking whether a variable is empty (or unset)</h2>
    <p>These checks will work independently of whether unset variable references are allowed (see 
    <a href="#referencing-unset">Referencing unset variables</a>).</p>
    <h3 id="empty-or-unset">Without distinction between unset and empty ( 
    <span class="code">${VAR:-}</span>)</h3>
    <p>This works whether the reference is 
    <span class="code">"</span>-quoted or not:</p>
    <pre class="code">
if test -z "${VAR:-}"; then
  echo "VAR is empty (or unset)"
elif test -n "${VAR:-}"; then
  echo "VAR is ${VAR}"
else
  echo "Not possible!"
fi
</pre>
    <h3 id="non-empty-empty-unset">Distinction between Non-empty, Empty, and Unset</h3>
    <p>Suppose you want to make the distinction between a variable being:</p>
    <ol>
      <li>Set and non-empty, e.g. 
      <span class="code">"Hello world!"</span> ( 
      <span class="code">${VAR}</span>)</li>
      <li>Set but empty: 
      <span class="code">""</span> ( 
      <span class="code">${VAR:-}</span>), or</li>
      <li>Unset ( 
      <span class="code">${VAR-...}</span>)</li>
    </ol>
    <p>This can be done as follows:</p>
    <pre class="code">
if test "${VAR-var_is_unset}" == "var_is_unset"; then
  echo "VAR is unset"
elif test -z "${VAR:-}"; then
  echo "VAR is empty"
else
  echo "VAR is ${VAR}"
fi
</pre>
    <p>With the (minor?) flaw that if the value of 
    <span class="code">VAR</span> is 
    <span class="code">var_is_unset</span>, the variable is erroneously considered unset.</p>
    <h2 id="names">Variable names</h2>
    <p>Variable names are case sensitive, and may contain numbers as well as 
    <span class="code">_</span>. The first character cannot be a number.</p>
    <p>By convention, global variable names are in all uppercase, e.g. 
    <span class="code">HOME</span>. Local variable names can be in lowercase to easily identify them.</p>
  </body>
</html>
