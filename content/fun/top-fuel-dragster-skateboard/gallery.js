/*! Copyright 2016 Morten Lauritsen Khodabocus, info@molk.ch
 *  Licensed under the GPL: http://molk.ch/licence.txt
 */

/*! global $ */
/*global $ */
"use strict";
$(function () {
    $('#gallery').fotorama({
	nav: 'thumbs',
	arrows: true
    });
});
