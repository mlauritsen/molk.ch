<?xml version="1.0" encoding="utf-8" standalone="no"?>
<?molk title="My SCRUM Guide" created="2019-04-04" ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../style/firefox/content.css" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../style/firefox/base.css" />
    <title>My SCRUM Guide (Ignored)</title>
  </head>
  <body id="content">
    <!--! Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
      Licensed under the GPL: http://molk.ch/licence.txt
  -->
    <p>This is a short guide to the SCRUM Framework. If you are just beginning to learn about SCRUM, don't read this - the official 
    <a href="http://www.scrumguides.org/scrum-guide.html">SCRUM guide</a> explains it better - and learning from an official 
    <a href="https://www.scrum.org/">scrum.org</a> trainer is even better! I wrote this guide to help me study for the SCRUM master certification.</p>
    <p class="summary">SCRUM is a deceptively 
    <em>simple framework</em> within which people can deliver 
    <em>valuable solutions</em> to 
    <em>complex problems</em>:</p>
    <dl class="summary">
      <dt>Deceptively simple</dt>
      <dd>Most people are tempted to tweak SCRUM, usually making changes to make it more complicated (the bigger the organisation, the more likely the urge to "customize") or more similar to existing habits. SCRUM leaves a lot of room for exactly 
      <em>how</em> work is accomplished (Pair program if you want, co-locate or distribute geographically), but the ground rules work best when left as-is. Particularly deceptive is the fact that if you leave out one part, other parts might not work as expected.</dd>
      <dt>Complex Problems</dt>
      <dd>For simple or well-understood problems, where no surprises will occur, SCRUM might be overkill. Just make sure you really want to bet the farm on the "no surprises" part.</dd>
      <dt>Valuable Solutions</dt>
      <dd>SCRUM is really good at making people focus on value. Because you are not planning into the distant hazy future, everything you do must have a clear relation to a concrete benefit for the customer. Don't design for complications that might arise later, just get it working now! This is a balance which must be kept - the "simple" in "the simplest thing that can possibly work" doesn't mean "the fewest lines of code", it means conceptually simple. Multiple components each with a clearly defined responsibility can be conceptually simpler than one component that does everything.</dd>
    </dl>
    <h2 id="foundation">Foundation</h2>
    <p>SCRUM is not a methodology. It does not mandate specific processes or techniques, but provides a framework in which processes and techniques can be employed (e.g. automated testing and QA, pair programming, etc.). The focus is on letting a motivated team with the necessary skills decide themselves how to achieve goals.</p>
    <h3 id="empiricism">Empiricism</h3>
    <p>SCRUM is based on "empirical process control", or 
    <a href="https://en.wikipedia.org/wiki/Empiricism">Empiricism</a>- the idea that in a complex world, best results are achieved through Transparency, Inspection, and Adaptation. When it is not possible to decide in advance how to best accomplish goals, experiments are performed, and the process evolves with experience. SCRUM provides the process through which these experiments and the resulting adaptations can be structured and maximise the likelihood that process improvements are regularly identified and implemented.</p>
    <h4 id="cone-of-uncertainty">Cone of Uncertainty</h4>
    <p>The 
    <a href="https://en.wikipedia.org/wiki/Cone_of_Uncertainty">Cone of Uncertainty</a> illustrates the fact that the less you know, the harder it is to plan. If a decision can wait until uncertainty has decreased, the risk of making the wrong decision decreases. Given enough time, many decisions will make themselves.</p>
    <h3 id="values">Values</h3>
    <p>The values of SCRUM are:</p>
    <dl>
      <dt>Focus</dt>
      <dd>Provide value 
      <em>right now</em>. Don't spend the first 6 months setting up, designing, and preparing for future features. Implement a simple feature, end-to-end, which the product owner can use as inspiration for the next-most important simple feature. This is particularly important in sprint 1: Do the absolute minimum which will allow you to deliver immediately.</dd>
      <dt>Courage</dt>
      <dd>Empirical process control is based on experimentation. Instead of spending a lot of time trying to get it right the first time, just try whatever seems most likely to work, and if it doesn't, inspect and adapt. When problems are identified, it is essential to have the courage to speak up, and to do what's necessary to address them.</dd>
      <dt>Commitment</dt>
      <dd>The team commits to a sprint goal, and the product owner and scrum master commit to shielding the team from outside interference.</dd>
      <dt>Respect</dt>
      <dd>Everybody in the SCRUM team respect the other team members and trust them to be skilled, motivated, and independent. This starts with the product owner and the SCRUM master trusting the team to self-organise when deciding who does what.</dd>
      <dt>Openness</dt>
      <dd>When something is not working, or when things go wrong, it is important to be open about exactly what went wrong. Team members must have the courage to be open about problems, and trust other team members to have done their best.</dd>
    </dl>
    <p>The values help build trust between all involved, which produces a positive feedback loop where trust reinforces the values.</p>
    <h2 id="framework">Framework</h2>
    <p>The SCRUM Framework consists of 
    <a href="#roles">roles</a>, 
    <a href="#rules">rules</a>, 
    <a href="#events">events</a> and 
    <a href="#artifacts">artifacts</a>.</p>
    <h3 id="roles">Roles</h3>
    <h4 id="role-scrum-master">SCRUM Master (SM)</h4>
    <p>When teams start using SCRUM, they often think "SCRUM Master" is just a new and slight variation on the usual "Project Manager" role. This is unfortunate, because the SCRUM Master is not a Manager. S/he is an enabler who:</p>
    <ul>
      <li>Helps the team self-organize</li>
      <li>Leads and coaches in the organization</li>
      <li>Plans SCRUM implementations</li>
      <li>Helps people understand and enact SCRUM</li>
      <li>Removes impediments for the SCRUM Team</li>
      <li>Increases artifact transparency (learning, convincing, change)</li>
    </ul>
    <p>Over time, and with continuous improvements to the process, the SCRUM Master ensures that the improvements do not conflict with SCRUM principles.</p>
    <h4 id="role-product-owner">Product Owner (PO)</h4>
    <p>The product owner provides direction and guidance, and ultimately controls the direction in which the product is heading. This is a vital role - and often the hardest for organizations to put in place. The PO should be:</p>
    <dl>
      <dt>Product Market Expert</dt>
      <dd>From a business perspective, intimate knowledge is required. Sometimes, the PO might have to clarify details (fair enough to want to sync with the people whose problems are being solved...), but generally, when a developer needs clarification for a task, the PO should be the obvious destination.</dd>
      <dt>Product Value Maximizer</dt>
      <dd>With limited resources, deciding what is the next "most valuable thing" to do is crucial. The PO must have a clear vision of where s/he wants to go, and make hard choices accordingly. This means deciding which minimal set of "small steps" will sum up to a "large step" in the right direction.</dd>
      <dt>Facilitator of Stakeholder Involvement</dt>
      <dd>There are two sides to stakeholder involvement - making sure everybody gets a say, and shielding the development team from noise. All requests and communication should be sync'ed with the PO, so s/he can make sure the focus stays on maximising value.</dd>
    </dl>
    <p>The team accepts 
    <em>no</em> requests which do not come from the PO. This ensures that priorities among 
    <em>all</em> stakeholders, not just the most vocal ones, are aligned, and the team can focus on what is 
    <em>truly</em> the current highest priority.</p>
    <h4 id="role-developer-team">Development Team</h4>
    <p>The developer team has 3 - 9 members (SM and PO do not count as members of the dev team). The SM and PO can also be developers, but this is not ideal, since conflicts of interest might occur. The team should be:</p>
    <dl>
      <dt>Self-organizing</dt>
      <dd>The team decides internally how to achieve goals, including who does what. It is important for the Scrum Master and the Product Owner to be enablers and facilitators - which does 
      <em>not</em> include having opinions about who should be assigned which task.</dd>
      <dt>Cross-functional</dt>
      <dd>The team possesses all skills required (Programmers, Architects, Business Analysts, Testers, Integrators, Support, Operators) to deliver the increment - minimize the number of team-external resources that can hinder progress.</dd>
      <dt>One Single Unit</dt>
      <dd>There are no "sub-teams", and the only role recognized is "developer". Anybody can participate in e.g. analysis and testing, which are typically areas where the volume of work varies depending on where the project is in the release cycle.</dd>
      <dt>Constantly Improving</dt>
      <dd>The team should always be looking to optimize creativity, productivity, and flexibility. The 
      <a href="#retrospective">Retrospective</a> is a regular opportunity to reflect, but the team is free to address impediments spontaneously.</dd>
    </dl>
    <p>Teams typically go through some steps before achieving a state of increased performance. Changing membership typically reduces cohesion, affecting performance and productivity in the short term.</p>
    <p>Whether a team is doing SCRUM or not, the concrete work that needs to be done consists of the same kinds of activities. The basic "things that need doing" are performed by similar roles internally in the development team:</p>
    <dl>
      <dt>Developer ("Coder")</dt>
      <dd>Perform the "real work", whether that is desigining software, writing code, laying bricks&#x2026;</dd>
      <dt>Business Analyst ("BA")</dt>
      <dd>Support the PO in specifying exactly what needs to be done.</dd>
      <dt>Project Manager ("PM")</dt>
      <dd>Coordinate with other teams, interact with corporate entities, request and track required resources (component management, rights management, etc.). This is no longer a lead role, but external communication and coordination is still required.</dd>
      <dt>Tester</dt>
      <dd>Testing must be integrated in the team, as part of the daily work. Testing is not something that happens separately from development - if it's not tested, it does not work!</dd>
      <dt>Specialist</dt>
      <dd>Whenever special skills are frequently required, the Database Administrator, Software Integrator, Configuration Manager, Networking, Security, etc. should be part of the team.</dd>
    </dl>
    <h3 id="rules">Rules</h3>
    <h4 id="timeout">Timeout</h4>
    <p>
    <a href="https://en.wikipedia.org/wiki/Time_is_of_the_essence">Time is of the essence</a>- in order to reduce waste and maintain focus on the sprint goal, it is vital that every team member understands why they do what they do. At any point in any event, discussion or meeting, if somebody feels like their time could be better spent otherwise, they raise their hand. Whenever you see somebody with their hand raised, stop what you are doing, and raise your own hand. This should not be taken personally by the speaker, but simply serves as a reality check - is the current activity relevant for all those present?</p>
    <h3 id="events">Events</h3>
    <p>SCRUM Meetings are time-boxed, which mean they have a maximum duration. There is no minimum duration - if the meeting is done before the time-box expires, that's great, back to work! If topics in a meeting exceed the allotted time, take them offline, to allow the meeting to progress. Alternatively, if issues are identified or clarification is required, which require action or investigation before the meeting can progress, abort the meeting and reschedule.</p>
    <p>External Stakeholders only participate in the sprint review - the PO represents them in other relevant meetings. Any member of the SCRUM team can interact freely with stakeholders otherwise.</p>
    <p>Additional meetings are fine if they facilitate achieving the sprint goal. Make sure to listen when people say they have "too many meetings" in the retrospective - all meetings should be obviously useful to all participants!</p>
    <p>Reduce the number of participants in meetings to the absolute minimum - there is nothing less motivating than being caught in a meeting where your only contribution is physical presence!</p>
    <table summary="SCRUM Events">
      <caption>These are the only events/meetings required in SCRUM.</caption>
      <tr>
        <th>Meeting</th>
        <th>Frequency</th>
        <th>Purpose</th>
        <th>Duration</th>
        <th>Participants</th>
        <th>Inputs</th>
        <th>Outputs</th>
      </tr>
      <tr>
        <th>Planning</th>
        <td>Beginning of each sprint</td>
        <td>Plan the next Sprint</td>
        <td>Max 8 hours (but typically 1-2 hours)</td>
        <td>The SCRUM Team</td>
        <td>
          <ul>
            <li>Current Increment</li>
            <li>Product Backlog</li>
            <li>Capacity</li>
            <li>Velocity</li>
          </ul>
        </td>
        <td>
          <ul>
            <li>Sprint Goal</li>
            <li>Sprint Backlog</li>
            <li>Concrete plan for how to achieve the sprint goal</li>
          </ul>
        </td>
      </tr>
      <tr>
        <th>Daily SCRUM</th>
        <td>Every day at the same time in the same place</td>
        <td>Track progress towards sprint goal</td>
        <td>Max 15 Minutes (typically 1 minute / participant)</td>
        <td>
          <ul>
            <li>Active: Developer Team</li>
            <li>Passive: Anybody else</li>
          </ul>
        </td>
        <td>Per participant: 
        <ul>
          <li>What did I do since the last daily?</li>
          <li>What will I do until the next daily?</li>
          <li>Is anything keeping me from progressing towards the Sprint Goal?</li>
        </ul> These are guideline questions, not a prescription.</td>
        <td>Impediments that must be addressed</td>
      </tr>
      <tr>
        <th>Review</th>
        <td>End of each sprint</td>
        <td>
          <ul>
            <li>Demo of what was implemented in the last sprint</li>
            <li>Get feedback from stakeholders</li>
            <li>Determine the focus of the next sprint</li>
          </ul>
        </td>
        <td>Max 4 hours (typically 1 hour)</td>
        <td>
          <ul>
            <li>SCRUM Team</li>
            <li>Stakeholders</li>
          </ul>
        </td>
        <td>All work accomplished up to and including the current Increment</td>
        <td>Probable content of the next sprint</td>
      </tr>
      <tr>
        <th>Retrospective</th>
        <td>End of each sprint</td>
        <td>Identify action points to increase efficiency/productivity</td>
        <td>Max 3 hours (typically &#x00BD;-1 Hour)</td>
        <td>The SCRUM Team</td>
        <td>All team members should prepare a list of things that went well and things that aren't going so well</td>
        <td>Pain points that must be addressed (SM in lead)</td>
      </tr>
    </table>
    <h4 id="sprint-planning">Sprint Planning</h4>
    <p>The PO presents the contents of the next sprint. All stories must meet the 
    <a href="#definition-of-ready">definition of ready</a>, particularly, they must be specified to a degree where any developer can grab them and start implementing, without needing to ask questions. Also, the stories must be estimated, so the team can confidently commit to finish the sprint backlog by the end of the sprint.</p>
    <h4 id="daily-scrum">Daily (SCRUM)</h4>
    <p>Each developer team member briefly reports what was done since the previous daily, what will be worked on until the next daily, and which problems might hinder progress. Activities that have nothing to do with the current sprint are not relevant - this is not a status report to the team lead, so no justification is required. Focus on progress and problems. If there is nothing to say, shut up!</p>
    <p>Every member of the team can raise their hand if they have the impression the daily is drifting off topic. If somebody raises their hand while you are speaking, that is valuable feedback - somebody does not understand why what you are saying is relevant to the current sprint goal. Either clarify, or cut it short.</p>
    <h4 id="review">Review (Demo)</h4>
    <p>This is where the team proudly presents what they have achieved in the current sprint. The focus should be on live demos - working software, not slides or status updates in tools.</p>
    <p>Stakeholders are welcome, and the goal of the next sprint is an obvious point of discussion after seeing what was done in the current sprint.</p>
    <h4 id="retrospective">Retrospective</h4>
    <p>The entire SCRUM team discusses what went well and what did not work so well. There are many different approaches to retrospectives, and changing the approach often can help keep people's attention. If people don't actively participate, or even suggest cancelling the retrospective, this is a warning sign - lethargy is often the result of too many retrospectives with the same issues being raised and nothing changing.</p>
    <p>If there are no big problems to address, that's great, but that just means you can focus on solving smaller issues. Something which is a small, easily fixable problem today could become a big problem if left unattended.</p>
    <h3 id="artifacts">Artifacts</h3>
    <h4 id="product-backlog">Product Backlog</h4>
    <p>The Product Backlog is a sorted list of known requirements of the product. Nothing gets done unless it is represented in the backlog. The product backlog does not have to be complete, and the items can be at any level of detail. The PO owns the product backlog and decides on the order of items in it.</p>
    <h4 id="sprint-backlog">Sprint Backlog</h4>
    <p>During the 
    <a href="#sprint-planning">sprint planning</a>, the team decides which items from the 
    <a href="#product-backlog">product backlog</a> will be included in the backlog for the next sprint. These items must be 
    <a href="#definition-of-ready">ready for inclusion</a>, in particular, since the team commits to finishing these items in the sprint, the item must be understood well enough for the team to give a reliable estimate.</p>
    <h4 id="backlog-item">Backlog Item</h4>
    <p>Often correspond to Stories, Use Cases, Epics, Features, etc. in other methods. An item can be at any level of maturity to be in the 
    <a href="#product-backlog">product backlog</a>, but must meet the 
    <a href="#definition-of-ready">definition of ready</a> to be accepted into a 
    <a href="#sprint-backlog">sprint backlog</a>. An item in a sprint is only considered done when it meets the 
    <a href="#definition-of-done">definition of done</a>.</p>
    <h4 id="definition-of-done">Definition of Done</h4>
    <p>Since SCRUM puts the focus on delivering value, it is important to only consider an item done when real value has been provided. This means all tasks related to the feature must be finished: Testing, refactoring, integration into the main codebase, quality assurance checks, deployment to an environment representative of production. The definition of what this means must be agreed on by people with very different perspectives: All members of the SCRUM team must agree. The PO must make sure that the definition is acceptable to external stakeholders.</p>
    <h4 id="definition-of-ready">Definition of Ready</h4>
    <p>What does it mean for an item in the 
    <a href="#product-backlog">product backlog</a> to be ready for inclusion into a 
    <a href="#sprint-backlog">sprint backlog</a>? It is important to agree on what will be accomplished when the item is complete. Crucial is that the development team understand the item well enough to provide an estimate of how long it will take.</p>
  </body>
</html>
