/*! Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
 *  Licensed under the GPL: http://molk.ch/licence.txt
 */

/*! global $, QUnit */
/*global $, QUnit */
"use strict";
$(function(){
    // Counters
    var nTags = -1,
        nItems = -1,
        nGreenItems = -1,
        nGreenItemsWithParents = -1,
        nRedItemsWithParents = -1,
        nGreenAndRedItemsWithParents = -1;

    // Utils
    function path(node) {
        var parentNames = [];
        node.parents().each(function() {
            var parentName = $(this).prop("tagName").toLowerCase(),
                index = $(this).index();
            if ($(this).is('[id]')) {
                parentName += "#" + $(this).attr('id');
            }
            parentName +="["+ index +"]";
            parentNames.push(parentName);
        });
        return parentNames.reverse().join("/");
    }

    // Test functions

    function resetNotVisible(assert) {
        assert.expect(1);
        assert.equal($('#reset').css('display'), 'none');
    }

    function allItemsVisible( assert ) {
        assert.expect(4 * nItems);
        $("#links .item").each(function() {
            assert.ok( $(this).hasClass('selected'), "All selected: " + path($(this)));
            assert.notOk( $(this).hasClass('unselected'), "None unselected: " + path($(this)));

            assert.ok( $(this).hasClass('unhighlighted'), "All unhighlighted: " + path($(this)));
            assert.notOk( $(this).hasClass('highlighted'), "None highlighted: " + path($(this)));
        });
    }

    function noFiltersActive( assert ) {
        assert.expect(2 * nTags);
        $("#links-tag-filter .tag").each(function() {
            assert.ok( $(this).hasClass('unfilter'), "All unfiltered: " + path($(this)));
            assert.notOk( $(this).hasClass('filter'), "None filtered: " + path($(this)));
        });
    }

    function thereAreNItemsTaggedGreen(assert) {
        assert.expect(1);
        $("#links-tag-filter #Green").each(function() {
            assert.ok(
		$(this).text().indexOf("(" + nGreenItems + ")"),
                "Text: '" + $(this).text() + "' Location: " + path($(this))
            );
        });
    }

    function greenTagIsFiltered(assert) {
        assert.expect(2);
        $("#links-tag-filter #Green").each(function() {
            assert.ok($(this).hasClass('filter'), "Filter: " + path($(this)));
            assert.notOk($(this).hasClass('unfilter'), "Not unfilter: " + path($(this)));
        });
    }

    function otherTagIsNotFiltered(assert) {
        assert.expect(2);
        $("#links-tag-filter #Orange").each(function() {
            assert.notOk($(this).hasClass('filter'), "Not filter: " + path($(this)));
            assert.ok($(this).hasClass('unfilter'), "Unfilter: " + path($(this)));
        });
    }

    function greenItemsAreSelected(assert) {
        assert.expect(2 * nGreenItemsWithParents);
        $("#links .item.Green").each(function() {
            assert.ok($(this).hasClass('selected'),      "selected: "   + path($(this)));
            assert.notOk($(this).hasClass('unselected'), "Unselected: " + path($(this)));
        });
    }

    function noItemsAreHighlighted(assert) {
        assert.expect(2 * nItems);
        $("#links .item").each(function() {
            assert.ok($(this).hasClass('unhighlighted'), "Unhighlighted: " + path($(this)));
            assert.notOk($(this).hasClass('highlighted'),   "Highlighted: "   + path($(this)));
        });
    }

    function resetIsVisible(assert) {
        assert.expect(1);
        assert.equal($('#reset').css('display'), 'block');
    }

    function otherItemsAreNotSelected(assert) {
        assert.expect(2);
        $("#links .Orange").each(function() {
            assert.notOk($(this).hasClass('selected'), "Not selected: " + path($(this)));
            assert.ok($(this).hasClass('unselected'), "Unselected: " + path($(this)));
        });
    }

    function thereAreSevenHiddenItems(assert) {
        assert.expect(1);
        assert.ok(
            $("#links-tag-filter #reset").text().indexOf("(7 hidden)"),
            "Location: " + path($("#links-tag-filter #reset"))
        );
    }

    function thereAreFourHiddenItems(assert) {
        assert.expect(1);
        assert.ok(
            $("#links-tag-filter #reset").text().indexOf("(4 hidden)"),
            "Location: " + path($("#links-tag-filter #reset"))
        );
    }    
    // Setup

    function countItems() {
        nTags = $("#links-tag-filter .tag").length;
        nItems = $("#links .item").length;
        nGreenItems = $("#links li.item.Green").length;
        nGreenItemsWithParents = $("#links .item.Green").length;
        nRedItemsWithParents = $("#links .item.Red").length;
        nGreenAndRedItemsWithParents = $("#links .item.Green.Red").length;
    }

    function setup() {
        $("#links").tagFilter({
            countSelector: 'li.item'
        });
        countItems();
    }

    // Tests

    QUnit.module( "Initial state", {
        beforeEach: function() {
            setup();
            console.log("Tag count: " + nTags +
                        " Item count: " + nItems +
                        " Green Count: " + nGreenItems +
                        " Green with parents count: " + nGreenItemsWithParents);
        }
    });

    QUnit.test( "Reset not visible", resetNotVisible);
    QUnit.test( "All items visible", allItemsVisible);
    QUnit.test( "No filters active", noFiltersActive);
    QUnit.test( "There are nGreenItems items tagged Green", thereAreNItemsTaggedGreen);

    QUnit.module( "One tag selected", {
        beforeEach: function() {
            // given
            setup();

            // when
            console.log("Triggering green click");
            $("#links-tag-filter #Green a").click();
        }
    });

    QUnit.test( "Green tag is filtered", greenTagIsFiltered);
    QUnit.test( "Other tag is not filtered", otherTagIsNotFiltered);
    QUnit.test( "Green items are selected", greenItemsAreSelected);
    QUnit.test( "No items are highlighted", noItemsAreHighlighted);
    QUnit.test( "Reset is visible", resetIsVisible);
    QUnit.test( "Other items are not selected", otherItemsAreNotSelected);
    QUnit.test( "There are 7 hidden items", thereAreSevenHiddenItems);

    QUnit.module( "Two tags selected", {
        beforeEach: function() {
            // given
            setup();

            // when
            console.log("Triggering Green click");
            $("#links-tag-filter #Green a").click();
            console.log("Triggering Red click");
            $("#links-tag-filter #Red a").click();
        }
    });

    QUnit.test( "Other tags are not filtered", otherTagIsNotFiltered);
    QUnit.test( "Reset is visible", resetIsVisible);
    QUnit.test( "Other items are not selected", otherItemsAreNotSelected);

    QUnit.test( "Both tags are filtered", function( assert ) {
        assert.expect(4);
        $("#links-tag-filter #Green").each(function() {
            assert.ok($(this).hasClass('filter'), "Filter: " + path($(this)));
            assert.notOk($(this).hasClass('unfilter'), "Not unfilter: " + path($(this)));
        });
        $("#links-tag-filter #Red").each(function() {
            assert.ok($(this).hasClass('filter'), "Filter: " + path($(this)));
            assert.notOk($(this).hasClass('unfilter'), "Not unfilter: " + path($(this)));
        });
    });

    QUnit.test( "Items of either tag are selected", function( assert ) {
        assert.expect(2 * (nGreenItemsWithParents + nRedItemsWithParents));
        $("#links .item.Green").each(function() {
            assert.ok($(this).hasClass('selected'),      "selected: "   + path($(this)));
            assert.notOk($(this).hasClass('unselected'), "Unselected: " + path($(this)));
        });
        $("#links .item.Red").each(function() {
            assert.ok($(this).hasClass('selected'),      "selected: "   + path($(this)));
            assert.notOk($(this).hasClass('unselected'), "Unselected: " + path($(this)));
        });
    });

    QUnit.test( "Items with both tags are highlighted", function( assert ) {
        assert.expect(2 * nGreenAndRedItemsWithParents);
        $("#links .item.Green.Red").each(function() {
            assert.ok($(this).hasClass('highlighted'),      "Highlighted: "   + path($(this)));
            assert.notOk($(this).hasClass('unhighlighted'), "Unhighlighted: " + path($(this)));
        });
    });

    QUnit.test( "Items with either tag but not both are not highlighted", function( assert ) {
        assert.expect(2 * (6 + 5));
        $("#links .item").each(function() {
            var isGreen = $(this).hasClass('Green'),
                isRed = $(this).hasClass('Red'),
                isHighlighted = $(this).hasClass('highlighted'),
                isUnhighlighted = $(this).hasClass('unhighlighted'),
                isGreenXorRed = isGreen ? !isRed : isRed;
            if (isGreenXorRed) {
                assert.ok($(this).hasClass('unhighlighted'),  "Unhighlighted: "   + path($(this)));
                assert.notOk($(this).hasClass('highlighted'), "Not highlighted: " + path($(this)));
            }
        });
    });

    QUnit.test( "There are 4 hidden items", thereAreFourHiddenItems);

    QUnit.module( "State after reset", {
        beforeEach: function() {
            // given
            setup();
            console.log("Triggering green click");
            $("#links-tag-filter #Green a").click();

            // when
            console.log("Triggering reset click");
            $("#links-tag-filter #reset a").click();
        }
    });

    QUnit.test( "Reset not visible", resetNotVisible);
    QUnit.test( "All items visible", allItemsVisible);
    QUnit.test( "No filters active", noFiltersActive);

    QUnit.module( "Initial filter: Green", {
        beforeEach: function() {
            // given
            $("#links").tagFilter({
                countSelector: 'li.item',
                filters: 'Green'
            });
            countItems();
        }
    });

    QUnit.test( "Green tag is filtered", greenTagIsFiltered);
    QUnit.test( "Other tag is not filtered", otherTagIsNotFiltered);
    QUnit.test( "Green items are selected", greenItemsAreSelected);
    QUnit.test( "No items are highlighted", noItemsAreHighlighted);
    QUnit.test( "Reset is visible", resetIsVisible);
    QUnit.test( "Other items are not selected", otherItemsAreNotSelected);
    QUnit.test( "There are 7 hidden items", thereAreSevenHiddenItems);
});
