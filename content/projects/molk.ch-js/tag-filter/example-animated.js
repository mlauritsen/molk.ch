/*! Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
 *  Licensed under the GPL: http://molk.ch/licence.txt
 */

/*! global $ */
/*global $ */
"use strict";
$(function(){
    $("#links").tagFilter({
        selected: function(elements) { elements.slideDown('fast'); },
        unselected: function(elements) { elements.slideUp('fast'); },
        highlighted: function(elements) { elements.animate({ fontSize: '2em' }); },
        unhighlighted: function(elements) { elements.animate({ fontSize: '1em' }); },
        filtered: function(elements) { elements.animate({ opacity: '1.0' }); },
        unfiltered: function(elements) { elements.animate({ opacity: '0.5' }); },
        itemSelector: ' a'
    });
});
