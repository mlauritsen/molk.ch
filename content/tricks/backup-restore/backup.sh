#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
set -o nounset
set -o errexit
trap 'echo "Error on line ${LINENO}: exited with status ${?}" >&2' ERR

# check that all files to be backed up map to actual files
files="backup.sh backup-test.sh backup.txt backup-test.key restore.sh $(cat backup.txt)"
echo -n 'Backing up:'
for file in ${files}; do
  if ! ls -d "${file}" &> /dev/null; then
    echo -e "\nFile not found: '${file}'" >&2
    exit 42
  fi
  echo -n " ${file}"
done
echo

# should accept either 0 or 2 arguments
if test $# -eq 0; then
  # to avoid typing the passphrase three times, prompt once and keep it in mem; less safe but practical
  read -s -p 'Enter GPG Username: ' username; echo
  read -s -p 'Enter GPG Password: ' password; echo
elif test $# -eq 2; then
  # this is completely unsafe; use for testing only (see backup-test.sh)!
  username=$1
  password=$2
else
  echo -e "\nUsage: $0 GnuPG-username GnuPG-password\nwas: $0 $*" >&2
  exit 42
fi
declare -r username password

# GPG functions
function molk_encrypt {
  local file=$1
  local encrypted="${file}.gpg"

  # fail if encrypted file exists
  if test -e "${encrypted}"; then
    echo -e "Could not encrypt '${file}': ${encrypted} exists" >&2
    exit 42
  fi

  if ! gpg --batch -r "${username}" --encrypt "${file}" &> /dev/null; then
    echo -e "Could not encrypt '${file}'" >&2
    exit 42
  fi
}

# identical in backup.sh and restore.sh
function molk_decrypt {
  local file=$1
  local decrypted_file=$2

  # fail if decrypted file exists
  if test -e "${decrypted_file}"; then
    echo -e "Could not decrypt '${file}': ${decrypted_file} exists" >&2
    exit 42
  fi
  if ! echo "${password}" | gpg --batch --passphrase-fd 0 --output "${decrypted_file}" --decrypt "${file}" 2> /dev/null; then
    echo -e "Could not decrypt '${file}'" >&2
    exit 42
  fi
}

# list the filename and human-readable size
function molk_created {
  local file=$1
  echo "Created ${file}: $(ls -hl ${file} | cut -d ' ' -f5)"
}

# check that GPG credentials are valid
test_file=/tmp/test.txt
test_file_gpg="${test_file}.gpg"
echo "Test" > "${test_file}"
rm -f "${test_file_gpg}"
if ! molk_encrypt "${test_file}"; then
  echo "Encryption failed - invalid username ('${username}')?" >&2
  exit 42
fi

rm -f "${test_file}"
if ! molk_decrypt "${test_file_gpg}" "${test_file}"; then
  echo "Decryption failed - invalid password ('${password}')?" >&2
  exit 42
fi
unset test_file test_file_gpg

# tarred file should not exist
declare -r tarred_file="ml-backup-$(date +%Y.%m.%d-%H.%M).tar"
if test -e "${tarred_file}"; then
  echo "Tarred file exists: '${tarred_file}'" >&2
  exit 42
fi

# compressed file should not exist
declare -r compressed_file="${tarred_file}.bz2"
if test -e "${compressed_file}"; then
  echo "Compressed file exists: '${compressed_file}'" >&2
  exit 42
fi

# encrypted file should not exist
declare -r encrypted_file="${compressed_file}.gpg"
if test -e "${encrypted_file}"; then
  echo "Encrypted file exists: '${encrypted_file}'" >&2
  exit 42
fi

# tar
echo; banner 'Archiving'
echo "Creating archive ${tarred_file}..."
for file in ${files}; do
  echo "  Adding '${file}'"
  if ! echo ${file} | xargs tar rvf "${tarred_file}" > /dev/null; then
    echo -e "\nCould not add '${file}' to ${tarred_file}" >&2
    exit 42
  fi
done

# test tar
if ! tar tf "${tarred_file}" > /dev/null; then
  echo "Error while parsing tarred file: '${tarred_file}'" >&2
  exit 42
fi

molk_created "${tarred_file}"

# compress
echo; banner 'Zipping'
echo "Compressing ${tarred_file} to ${compressed_file}..."
if ! bzip2 --verbose "${tarred_file}"; then
  echo "Error while compressing file: '${tarred_file}'" >&2
  exit 42
fi

# test compress
echo "Testing compressed file..."
if ! bzip2 --test "${compressed_file}" > /dev/null; then
  echo "Error while testing compressed file: '${compressed_file}'" >&2
  exit 42
fi

molk_created "${compressed_file}"

# encrypt
echo; banner 'Encrypting'
echo "Encrypting ${compressed_file} to ${encrypted_file}..."
molk_encrypt "${compressed_file}"
rm "${compressed_file}"

molk_created "${encrypted_file}"

# test restore
echo; banner 'Testing'
echo "Testing restore of ${encrypted_file}..."
if ! ./restore.sh "${username}" "${password}" "${encrypted_file}"; then
  echo "Failed to restore ${encrypted_file}" >&2
  exit 42
fi

# test file integrity
if ! test -z "$(diff ./backup.sh ./restore/backup.sh)"; then
  echo "File integrity check failed: ./backup.sh ./restore/backup.sh" >&2
  diff -u ./backup.sh ./restore/backup.sh >&2
  exit 42
fi

# cleanup
echo; banner 'Cleaning'
rm -rf ./restore/
exit 0
