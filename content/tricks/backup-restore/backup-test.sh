#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt
set -o nounset
set -o errexit
trap 'echo "Error on line ${LINENO}: exited with status ${?}" >&2' ERR

# setup
tmp=/tmp/$(echo backup-test-$RANDOM)
if ! mkdir "${tmp}"; then
  echo "Could not create tmp"
  exit 43
fi
cp ./backup.sh ./backup-test.sh ./backup-test.key ./restore.sh "${tmp}"
cd "${tmp}"

# assumes existence of the GPG username+passphrase
# see http://molk.ch/tips/gnu/privacy-guard.html
username=$(basename ${0%.sh})
password=${username}

# test KO

if ./backup.sh "${username}" "${password}" &> /dev/null; then
  echo "missing ./backup.txt should fail"
  exit 43
fi

filename=file.txt
file="${tmp}/${filename}"
echo "${filename}" > "${tmp}/backup.txt"

if ./backup.sh "${username}" "${password}" &> /dev/null; then
  echo "backup.txt entry not found should fail"
  exit 43
fi

echo "Hello World!" > "${file}"

if ./backup.sh only-one-argument &> /dev/null; then
  echo "should fail when one argument is passed"
  exit 43
fi

if ./backup.sh 1 2 3 &> /dev/null; then
  echo "should fail when more than two arguments are passed"
  exit 43
fi

if ./backup.sh unknown-user "${password}" &> /dev/null; then
  echo "should fail when invalid gpg uid is passed"
  exit 43
fi

if ./backup.sh "${username}" invalid-password &> /dev/null; then
  echo "should fail when invalid gpg password is passed"
  exit 43
fi

# test OK

if ! ./backup.sh "${username}" "${password}" &> /dev/null; then
  echo "should pass"
  exit 43
fi

# this can fail if the second changes between the previous run and this one (remove seconds from backup name?)
if ./backup.sh "${username}" "${password}" &> /dev/null; then
  echo "should fail when backup already exists"
  exit 43
fi

# teardown

if ! rm -rf "${tmp}"; then
  echo "Could not remove tmp directory '${tmp}'"
  exit 43
fi

echo "tests OK: $0"
