<?xml version="1.0" encoding="utf-8" standalone="no"?>
<?molk title="Backup and Restore" created="2010-09-23" ?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../style/firefox/content.css" />
    <link rel="stylesheet" type="text/css" title="Development" href="../../../style/firefox/base.css" />
    <title>Backup and Restore (Ignored)</title>
  </head>
  <body id="content">
    <!--! Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
      Licensed under the GPL: http://molk.ch/licence.txt
  -->
    <p class="summary">Very simple backup (and restore!) script(s). Since I tend to run the backup at the end of sessions, and just leave it running, it does not matter how inefficient and slow it is, but once it gets going, it should not expect me to answer questions.</p>
    <p>Not being able to ask questions is something encryption tools are unfond of - they don't generally like passwords/passphrases to be provided from anything but the keyboard. However, my script requires the passphrase to be provided three times (once for validation, once for encryption, and once for the test of the restore). The script takes a while to run (see the sample run below...), and I don't want to sit there and wait for it to require my password yet again.</p>
    <p>The backup script produces one file, that I can keep in whichever safe place I please: USB drive, online storage, or even in my home directory, as a safety net when I'm making changes that I might want to undo.</p>
    <h2 id="objectives">Objectives</h2>
    <ul>
      <li>Why roll my own? There are many solutions to this problem out there, but I wanted something that was "one-click" (actually, "single-command"), low-tech, (vendor- and tool-) independent, and with few dependencies on the environment - neither a network connection nor anything more than a simple console should be required. It can't get much simpler than bash, tar and bzip2.</li>
      <li>Adding 
      <a href="/tips/gnu/privacy-guard.html">GnuPG</a> encryption made things quite a bit less simple, but seeing as the purpose of the backup is to keep it outside of my encrypted home directory, encrypting the backup really is a must.</li>
      <li>A simple text terminal has to be sufficient to run the backup. This might be all that is available if the system breaks unexpectedly, in which case, backing up data is the first thing to do.</li>
      <li>
        <p>The restore has to work, no matter what happens. With a simple archive / compress / encrypt strategy, a manual restore is no problem, all that is required is the backup file and appropriate GPG credentials - the restore script is more convenient, but not necessary.</p>
        <p>Saying that "the restore has to work" sounds obvious, but it's not. I have worked on several projects where regular backups were considered an obvious must, but nobody bothered doing regular test restores. This makes for 
        <a href="https://en.wikipedia.org/wiki/May_you_live_in_interesting_times">interesting times</a> when something goes wrong, and a backup exists, but the restore is not working...</p>
      </li>
      <li>Every possible effort should be made to ensure that no errors go undetected during the backup.</li>
      <li>Last but not least, the script must be 
      <a href="#testing">testable</a>. This was one of the reasons why GPG was a good solution for encryption - although discouraged, it let's you script everything if you insist.</li>
    </ul>
    <h2 id="backup">The 
    <a href="backup.sh">Backup Script</a></h2>
    <p>The essence of the script is ( 
    <a href="http://en.wikipedia.org/wiki/Pseudocode">pseudo-code</a>):</p>
    <pre class="code">
tar rvf       tar-file files-to-backup
bzip2         tar-file
gpg --encrypt tar-file.bz2
</pre>
    <p>Rigorous input parameter checking, internal sanity checks, and validation of the output make the script balloon up to 150+ lines.</p>
    <dl>
      <dt>Check GPG credentials</dt>
      <dd>Since the script is likely to run for a while before actually needing the credentials, it starts out by checking that the credentials will work with 
      <a href="/tips/gnu/privacy-guard.html">GnuPG</a> to encrypt/decrypt a file. No reason for the script to first create the archive and then fail because there was a credential typo.</dd>
      <dt>Check that the specified files to be archived ( 
      <span class="code">backup.txt</span>) exist</dt>
      <dd>Also, to fail-fast, the patterns specified in 
      <span class="code">backup.txt</span> are checked, to make sure they expand to actual files. A typo in that file could mean that important files are not included in the backup, which would be a bad thing.</dd>
      <dt>Create the archive</dt>
      <dd>Add all specified files to an appropriately timestamped tar-file.</dd>
      <dt>Compress the archive</dt>
      <dd>Use bzip2 to compress the archive. This is a separate step to facilitate swapping in another compression-tool.</dd>
      <dt>Check the compressed file</dt>
      <dd>Check that the compressed file is OK. There is no reason why it wouldn't be, but better safe than sorry.</dd>
      <dt>Encrypt the compressed file</dt>
      <dd>Since the backup can be stored pretty much anywhere - it's just a file, that's the whole point - 
      <a href="/tips/gnu/privacy-guard.html">encryption</a> is necessary..</dd>
      <dt>Perform a test restore of the encrypted file</dt>
      <dd>
        <p>Once the compressed archive has been encrypted, the backup itself is done. However, it is crucial that the 
        <a href="#restore">restore script</a> works, but I expect to be using it a lot less than the backup script. Hence, I decided to make validation of the restore procedure part of the backup procedure. This slows things down considerably, which, in this case, is worth it.</p>
        <p>After running the restore, the script checks that the restored version of a file which is known to be in the archive (the backup script) is identical to the original.</p>
      </dd>
      <dt>Cleanup</dt>
      <dd>Delete any files that have been generated and are no longer needed.</dd>
    </dl>
    <p>Errors are written to 
    <span class="code">stderr</span>.</p>
    <p>Progress info is intentionally kept sparse. The 
    <a href="http://packages.debian.org/lenny/sysvbanner">System V banner</a> utility is used to provide regular feedback which is readable from far away. This is practical since I tend to do other things away from my machine while the backup is running.</p>
    <h3 id="input">Input: 
    <a href="#backup.txt">
      <span class="code">backup.txt</span>
    </a></h3>
    <p>This file contains patterns matching the files that will be included in the backup:</p>
    <pre class="code">
Documents
.mozilla-thunderbird
.bashrc
.m2/settings.xml
</pre>
    <p>These can be any pattern that expands to filenames in the directory where the 
    <a href="#backup">Backup Script</a> is run, the only limitation is that all patterns must match existing files - otherwise, the backup script will fail. This is a simple 
    <a href="http://en.wiktionary.org/wiki/safeguard">safeguard</a> to avoid typos in a pattern causing files to not be backed up.</p>
    <h3 id="arguments">Arguments: GPG Credentials</h3>
    <p>The script optionally accepts the GPG username and password (a.k.a. "passphrase") as commandline arguments.</p>
    <p>This is a needed since I want to write 
    <a href="#automated-tests">automated tests</a> for the scripts - there's just no way to automate testing of a script that prompts you for a password.</p>
    <p>Convenience sometimes beats security - it is generally a bad idea to pass passwords as arguments, and unnecessarily storing them in variables is to be avoided as well. In this case, I only pass the test credentials as arguments (making sure they don't end up in the bash history, of course).</p>
    <p>If the credentials are not given as arguments, the script prompts. The advantage of having the backup script prompt rather than leaving it up to GPG is that my script only prompts for the credentials once. They are then saved, quite unsafely, to memory, for the duration of the backup. The convenience of only having to enter my passphrase once, while still ensuring that I didn't mistype it, was just too good to pass up. GPG would only be prompting after the backup archive was created, which might be hours after the script was launched. One typo, and it would be back to square one.</p>
    <h2 id="restore">The 
    <a href="#restore.sh">Restore Script</a></h2>
    <p>This is simply the 
    <a href="#backup">Backup script</a> in reverse order (also 
    <a href="http://en.wikipedia.org/wiki/Pseudocode">pseudo-code</a>):</p>
    <pre class="code">
gpg --decrypt backup-file.tar.bz2.gpg
bunzip2       backup-file.tar.bz2
tar xvf       backup-file.tar
</pre>
    <p>There's a lot less sanity checking and validation than in the 
    <a href="#backup">Backup script</a>- either the restore works or it doesn't.</p>
    <p>Errors and progress info is written to 
    <span class="code">stderr</span>.</p>
    <h2 id="testing">The 
    <a href="#backup-test.sh">Automated Test Script</a></h2>
    <p>Once you get used to unit testing, it becomes uncomfortable to write code without testing it. It is just so much more efficient to have a lean little test that will tell you whether your code is working. I experimented with bash unit testing on the 
    <a href="/projects/molk.ch-build">molk.ch build project</a>, and it worked really well.</p>
    <p>Hence, when writing something as important as the script that will ensure that my data does not go away if my system does, automated testing was not optional. My actual backup is quite big, and while fiddling with the gory details of making the backup script encrypt the backup file, it was very efficient to have a test script which gave me feedback within seconds of making a change.</p>
    <p>A major advantage over just backing up a couple of files manually on the commandline - which would have been another way to get quick feedback - is that the tests also work as executable documentation. The next time I modify the script, which might be years from now, the tests will tell me why the script works the way it does.</p>
    <p>The coverage of the test script is far from perfect. Some of the more exotic things that can go wrong, like the compressed file being corrupted, I did not bother to reproduce. The tests covers the most likely scenarios - the important part is not to test everything, but to test the parts that matter.</p>
    <h2 id="files">The files</h2>
    <p>These are the files making up the backup/restore solution:</p>
    <dl>
      <dt id="backup.sh">
        <a href="backup.sh">
          <span class="code">backup.sh</span>
        </a>
      </dt>
      <dd>This is 
      <a href="#backup">the script that performs the backup</a>.</dd>
      <dt id="restore.sh">
        <a href="restore.sh">
          <span class="code">restore.sh</span>
        </a>
      </dt>
      <dd>This is 
      <a href="#restore">the script that performs the restore</a>.</dd>
      <dt id="backup-test.sh">
        <a href="backup-test.sh">
          <span class="code">backup-test.sh</span>
        </a>
      </dt>
      <dd>Verifies that the backup-restore scripts are working.</dd>
      <dt id="backup.txt">
        <a href="backup.txt">
          <span class="code">backup.txt</span>
        </a>
      </dt>
      <dd>This is the file containing the 
      <a href="#input">patterns describing the files to backup</a>.</dd>
    </dl>
    <h2 id="sample-output">Sample output</h2>
    <p>The following is a sample run of the 
    <a href="#backup">backup script</a> ( 
    <span class="code">$</span> is the prompt, 
    <em>emphasized text</em> is typed by the user):</p>
    <pre class="code">
$ ./backup.sh test1 test1
Backing up: backup.sh backup-test.sh backup.txt backup-test.key restore.sh .bashrc

   #
  # #    #####    ####   #    #     #    #    #     #    #    #   ####
 #   #   #    #  #    #  #    #     #    #    #     #    ##   #  #    #
#     #  #    #  #       ######     #    #    #     #    # #  #  #
#######  #####   #       #    #     #    #    #     #    #  # #  #  ###
#     #  #   #   #    #  #    #     #     #  #      #    #   ##  #    #
#     #  #    #   ####   #    #     #      ##       #    #    #   ####

Creating archive ml-backup-2010.11.16-21.19.tar...
  Adding 'backup.sh'
  Adding 'backup-test.sh'
  Adding 'backup.txt'
  Adding 'backup-test.key'
  Adding 'restore.sh'
  Adding '.bashrc'
Created ml-backup-2010.11.16-21.19.tar: 20K

#######
     #      #    #####   #####      #    #    #   ####
    #       #    #    #  #    #     #    ##   #  #    #
   #        #    #    #  #    #     #    # #  #  #
  #         #    #####   #####      #    #  # #  #  ###
 #          #    #       #          #    #   ##  #    #
#######     #    #       #          #    #    #   ####

Compressing ml-backup-2010.11.16-21.19.tar to ml-backup-2010.11.16-21.19.tar.bz2...
  ml-backup-2010.11.16-21.19.tar:  3.181:1,  2.515 bits/byte, 68.56% saved, 20480 in, 6439 out.
Testing compressed file...
Created ml-backup-2010.11.16-21.19.tar.bz2: 6.3K

#######
#        #    #   ####   #####    #   #  #####    #####     #    #    #   ####
#        ##   #  #    #  #    #    # #   #    #     #       #    ##   #  #    #
#####    # #  #  #       #    #     #    #    #     #       #    # #  #  #
#        #  # #  #       #####      #    #####      #       #    #  # #  #  ###
#        #   ##  #    #  #   #      #    #          #       #    #   ##  #    #
#######  #    #   ####   #    #     #    #          #       #    #    #   ####

Encrypting ml-backup-2010.11.16-21.19.tar.bz2 to ml-backup-2010.11.16-21.19.tar.bz2.gpg...
Created ml-backup-2010.11.16-21.19.tar.bz2.gpg: 6.9K

#######
   #     ######   ####    #####     #    #    #   ####
   #     #       #          #       #    ##   #  #    #
   #     #####    ####      #       #    # #  #  #
   #     #            #     #       #    #  # #  #  ###
   #     #       #    #     #       #    #   ##  #    #
   #     ######   ####      #       #    #    #   ####

Testing restore of ml-backup-2010.11.16-21.19.tar.bz2.gpg...

 #####
#     #  #       ######    ##    #    #     #    #    #   ####
#        #       #        #  #   ##   #     #    ##   #  #    #
#        #       #####   #    #  # #  #     #    # #  #  #
#        #       #       ######  #  # #     #    #  # #  #  ###
#     #  #       #       #    #  #   ##     #    #   ##  #    #
 #####   ######  ######  #    #  #    #     #    #    #   ####
</pre>
  </body>
</html>
