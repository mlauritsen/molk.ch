#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
set -o nounset
set -o errexit
trap 'echo "Error on line ${LINENO}: exited with status ${?}" >&2' ERR

# setup

# There must be exactly three arguments
if test $# -ne 3; then
  echo -e "Usage: $0 username password encrypted-file\nWas: $0 $@" >&2
  exit 42
fi
declare -r username=$1
declare -r password=$2
declare -r encrypted_file=$3
declare -r compressed_file=${encrypted_file%.gpg}

# The encrypted file must be readable
if ! test -r "${encrypted_file}"; then
  echo 'Encrypted file ('"${encrypted_file}"') must be readable' >&2
  exit 42
fi

# identical in backup.sh and restore.sh
function molk_decrypt {
  local file=$1
  local decrypted_file=$2

  # fail if decrypted file exists
  if test -e "${decrypted_file}"; then
    echo -e "Could not decrypt '${file}': ${decrypted_file} exists" >&2
    exit 42
  fi

  if ! echo "${password}" | gpg --batch --passphrase-fd 0 --output "${decrypted_file}" --decrypt "${file}" 2> /dev/null; then
    echo -e "Could not decrypt '${file}'" >&2
    exit 42
  fi
}

# Create restore directory and change into it
if ! mkdir restore; then
  echo 'Could not create restore directory: '"${PWD}/restore" >&2
  exit 42
fi
cd restore

# decrypt
molk_decrypt "../${encrypted_file}" "./${compressed_file}"

# uncompress
tar xjf "./${compressed_file}"
