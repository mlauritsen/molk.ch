/*! Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
 *  Licensed under the GPL: http://molk.ch/licence.txt
 *
 * 2017-03-18 tag-filter v1.0: Filter list items by the tags listed in their class attributes.
 */

/*! global $ */
/*global $ */
"use strict";
$(function() {
    // Add jQuery command
    $.fn.tagFilter = function(options) {
        var state = { itemsRoot: $(this) },
            settings = $.extend( // optionally overridden default values
                { excludedTags: [],
                  selected: function(item) { },
                  unselected: function(item) { },
                  highlighted: function(item) { },
                  unhighlighted: function(item) { },
                  filtered: function(tag) { },
                  unfiltered: function(tag) { },
                  countSelector: '.item'
                },
                options || {});

        function toggle(selectedTags, tagName, tagElement) {
            var copy = [],
                i,
                j = 0,
                isFilter = true;
            console.log("Toggling "+tagName);

            // Copy the selectedTags, keeping track of whether the tagName was in it
            for (i = 0; i < selectedTags.length; i += 1) {
                if (selectedTags[i] === tagName) {
                    isFilter = false;
                } else {
                    copy[j] = selectedTags[i];
                    j += 1;
                }
            }

            // If tagName was not found, the filter is being added
            if (isFilter) {
                console.log(tagName+" was filtered");
                tagElement.addClass("filter").removeClass("unfilter");
                settings.filtered(tagElement);
                copy.push(tagName);
            } else { // The filter is being removed
                console.log(tagName+" was unfiltered");
                tagElement.addClass("unfilter").removeClass("filter");
                settings.unfiltered(tagElement);
            }

            // Replace selectedTags by the copy
            while (selectedTags.length > 0) {
                selectedTags.pop();
            }
            for (i = 0; i < copy.length; i += 1) {
                selectedTags[i] = copy[i];
            }
        }

        function hidden() {
            var count = state.itemsRoot.find(settings.countSelector + ".unselected").length;
            state.hidden.text(count);
            if (count === 0) {
                state.reset.css('display','none');
            } else {
                state.reset.css('display', 'block');
            }
        }

        function refresh() {
            // Setup
            var i,
                tag,
                nSelectedTags = state.selectedTags.length;
            console.log("Refreshing with filter: " + state.selectedTags);

            // If all tags are selected, then no tags are selected
            if (state.ntags === nSelectedTags) {
                state.selectedTags = [];
            }

            // If no tags are selected, select all items
            if (nSelectedTags === 0) {
                console.log("No filters, displaying all");
                state.items.addClass("selected").removeClass("unselected").removeClass("highlighted").addClass("unhighlighted");
                state.itemsRoot.find(".item.selected").each(function() {settings.selected($(this));});
                state.itemsRoot.find(".item.unhighlighted").each(function() {settings.unhighlighted($(this));});

                state.tags.removeClass("filter").addClass("unfilter");
                state.tagsRoot.find(".unfilter").each(function() {settings.unfiltered($(this));});
                hidden();
                return;
            }

            // Unselect
            state.items.removeClass("selected").removeClass("highlighted").addClass("unselected").addClass("unhighlighted");
            state.tags.removeClass("filter").addClass("unfilter");

            // Select by tag
            for (i = 0; i < state.ntags; i += 1) {
                tag = state.selectedTags[i];

                // Highlight items which are already selected
                state.itemsRoot
		    .find("." + tag + ".selected")
		    .removeClass("unhighlighted")
		    .addClass("highlighted");

                // Select the tagged items
                state.itemsRoot.find("." + tag)
		    .removeClass("unselected")
		    .addClass("selected");

                // Tags
                state.tagsRoot.find("#" + tag).removeClass("unfilter").addClass("filter");
            }

            // Select parents with selected children
            state.itemsRoot.find(".item.selected").parents(".item").removeClass("unselected").addClass("selected");

            // Apply dynamic styling
            state.itemsRoot.find(".item.selected").each(function() {settings.selected($(this));});
            state.itemsRoot.find(".item.unselected").each(function() {settings.unselected($(this));});
            state.itemsRoot.find(".item.highlighted").each(function() {settings.highlighted($(this));});
            state.itemsRoot.find(".item.unhighlighted").each(function() {settings.unhighlighted($(this));});
            state.tagsRoot.find(".filter").each(function() {settings.filtered($(this));});
            state.tagsRoot.find(".unfilter").each(function() {settings.unfiltered($(this));});
            hidden();
        }

        function initialFilters() {
            var filters = $.url('?filters');
            if (filters) {
                filters = filters.trim();
            } else if (settings.filters) {
                filters = settings.filters;
            } else {
                return [];
            }
            // URL and settings are treated the same
            return $.trim(filters).replace(/\s+/g, '').split(',');
        }

	function addTag(tag) {
            var tagElement,
		count = state.itemsRoot.find(settings.countSelector + "." + tag).length;
            // Add a tag link and toggle when clicked
            state.tagsRoot.append("<li id=\"" + tag + "\" class=\"tag unfilter\"><a href=\"#\">" + tag + "</a> (" + count + ")</li>");
            tagElement = state.tagsRoot.find("#" + tag);
            tagElement.on("click", "a", function() {
                console.log("Clicked "+tag);
                toggle(state.selectedTags, tag, tagElement);
                refresh();
                return false;
            });
        }

        function initialize() {
            var i,
		tagsRootId;
            console.log("Initializing");

            // 'item' indicates that the elements should be filtered, it is not a tag
            settings.excludedTags.push("item");

            console.log("Items root: "+state.itemsRoot.attr("id"));

            // selectedTags is an array containing the currently selected tags; default is none
            state.selectedTags = initialFilters();

            // Insert and cache the parent element of the filter tags
            tagsRootId = state.itemsRoot.attr("id") +"-tag-filter";
            console.log("Tags root id: "+tagsRootId);
            state.itemsRoot.before("<div id=\"" + tagsRootId + "\"><h2>Tags</h2><ul></ul></div>");
            state.tagsRoot = $("#" + tagsRootId + " ul");

            // items contains all the HTMLElements representing list items with tags
            state.items = state.itemsRoot.find(".item");

            // tagNames contains all tags on the items being filtered
            state.tagNames = [];
            state.items.each(
                function() {
                    // Treat multiple internal whitespaces as a single space
                    var tag,
			classes = $.trim($(this).attr('class')).replace(/\s\s+/g, ' ').split(' ');
                    for (i = 0; i < classes.length; i += 1) {
                        tag = classes[i];
                        console.log("Tag " + tag);

                        // ignore excluded tags
                        if ($.inArray(tag, settings.excludedTags) === -1) {
                            // children should inherit tags of their parents
                            $(this).find(".item").addClass(tag);

                            if($.inArray(tag, state.tagNames) === -1) {
                                console.log("Adding tag " + tag);
                                state.tagNames.push(tag);
                            }
                        }
                    }
                }
            );

            state.tagNames.sort();
            state.ntags = state.tagNames.length;

            // Init tag list
            for (i = 0; i < state.ntags; i += 1) {
                addTag(state.tagNames[i]);
	    }

	    // state.tags contains all the HTMLElements representing tag-selection LIs
	    state.tags = state.tagsRoot.find("li.unfilter");

	    // Add reset link
	    state.tagsRoot.append("<li id=\"reset\"><a href=\"#\">Reset</a> (<span>0</span> hidden)</li>");
	    state.reset = state.tagsRoot.find("#reset");
	    state.hidden = state.reset.find("span");
	    state.tagsRoot.find("#reset").bind("click", null,
					       function() {
                                                   state.selectedTags = [];
                                                   refresh();
                                                   return false;
					       }                                   
					      );
        }

        initialize();
        refresh();
        return true;
    };
});
