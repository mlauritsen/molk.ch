/*! Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
 *  Licensed under the GPL: http://molk.ch/licence.txt
 */

/*! global $ */
/*global $ */
"use strict";
$(function(){ $("#links").tagFilter({
    countSelector: 'a.item'
}); });
