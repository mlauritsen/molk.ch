#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

cp "${MOLK_SRC}/content/projects/molk.ch-js/tag-filter/tag-filter.src.js" "${MOLK_SRC}/content/javascript/tag-filter.js"
