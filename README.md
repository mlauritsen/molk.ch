Repository overview:

See also http://molk.ch/projects/molk.ch/index.html.

Files directly under root are "utilities", pages that contain
information about the site.

Images used in all pages are in content/images, e.g. the xhtml, css,
and atom standard conformance certificate images.

src/overview.html:

  Provides an overview (with links) of examples and other bits and
  pieces.

src/content:

  This is where the core content is located. The source pages are
  valid XHTML, but pure content - only the body is included in the
  final page. You only get to see the final layout after running the
  build, which encourages 1) focusing on the content when
  creating/updating pages, and 2) structuring the content
  flexibly. You have no control over the size of the reader's
  screen/browser window anyway, so this restriction does not make much
  of a difference, once you get used to it.

  The subdirectories of src/content each represent a section of the
  site, and define the first level of navigation (tabs at the top of
  the page). The next level of subdirectories define the second level
  of navigation (Listed on the right above the icons). There are only
  two explicit levels of navigation - after that, pages just link to
  each other, and the breadcrumbs indicate where in the hierarchy a
  page is located.

src/examples:

  When working on the page elements and layout, it is practical to
  have a minimal page with just the part which is being
  styled. See also src/index.html.

src/fragments:

  This directory contains:

    .html-files: Which are valid XHTML documents whose content will be
    included in all the content HTML files in the site. Usually, the
    contents of the body-element will be extracted, the exception
    being head.html, where the head-element is used.

    .tag-files: Which are parts of XHTML documents, and hence, not
    always neither valid nor wellformed.

  Together, these files minimise duplication between the core content
  files.

src/style:

  Each subdirectory represents a style for the site: Currently, a
  Firefox, Simple and a Print style exist. The src/style/lib/ directory
  contains thirdparty libraries. See also src/content/javascript.html.

Build (assumes cwd is the root of molk.ch):
  src/build.sh clean check-src
    Useful for quickly checking edited sources
  src/build.sh clean build
     clean: Delete the previously generated site
     format-src / check-src: Format sources / Format and check sources (does not include target checks)
     build: Generate the site locally
     deploy: Upload the generated files to molk.ch
     mirror: Download the site from molk.ch
     diff: Compare the downloaded site to the locally generated files

View site in browser:
  1) Run
       .../molk.ch-build/utils/serve.py -xhtml
     or
       .../molk.ch-build/utils/serve.py
     in the site root directory, e.g.
       (cd dest && ../../molk.ch-build/utils/serve.py -xhtml)
     from molk.ch/
  2) Point firefox to (serving with -xhtml, since self-closing tags are not allowed in HTML)
       http://localhost:8042/index.html
     or use lynx (which does not support XHTML, so serve without -xhtml...)
       lynx http://localhost:8042/index.html

Authoring content:
- To get an impression of what content will look like styled for molk.ch, add:
    <head>
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <link rel="stylesheet" type="text/css" title="Development" href="../../style/firefox/content.css" />
      <link rel="stylesheet" type="text/css" title="Development" href="../../style/firefox/base.css" />
      <title>Page Title (Ignored)</title>
    </head>

Custom MOLK checks:
  ./molk-checks.sh
