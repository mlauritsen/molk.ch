#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt
set -o nounset
set -o errexit
trap 'echo "Error on line ${LINENO}: exited with status ${?}" >&2' ERR

echo 'These checks are very specific to molk.ch, hence they are not part of the build, which is somewhat generic.'

echo -e '\nSpelling issues (developing, neccesary, mentioned, decipher, occasionally):'
for word in \
    'evelopp' \
    'eccessary' \
    'ecesary' \
    'eccesary' \
    'nned' \
    'dechipher' \
    'ccasional' \
; do \
  find content/ -name '*.html' -exec grep -Hi $word \{} \;; \
done

echo -e "\nAll pages should contain id=\"content\":"
for file in $(find content/ -name '*.html'); do
  if ! grep -q 'id="content"' "${file}"; then
     echo 'No id="content" found in '"${file}";
  fi
done

echo -e "\nAll pages should contain class=\"summary\":"
for file in $(find content/ -name '*.html'); do
  if ! grep -q 'class="summary"' "${file}"; then
     echo 'No class="summary" found in '"${file}";
  fi
done
