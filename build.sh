#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt
set -o nounset
set -o errexit

# this file can be used to invoke molk.ch-build from the base molk.ch directory
# e.g. ./build.sh clean site

# check parameters
if test $# -eq 0; then
  echo -e "\nUsage: $0 <targets>\nwas: $0 $*" >&2
  exit 42
fi

cd "${MOLK_BUILD}" || exit 42

time make ${@}
